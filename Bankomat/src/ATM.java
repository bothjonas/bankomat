
public class ATM {
	private double balance = 0;

	public double checkBalance() {
		return balance;
	}

	public double depositMoney(double amount) {
		balance = balance + amount;
		return balance;
	}

	public double withdrawMoney(double amount) {
		balance = balance - amount;
		return balance;
	}
	public String insufficientBalance() {
		String errorMessage = "You don't have enough Balance to withdraw that much money!";
		return errorMessage;
	}

	public ATM(double balance) {
		super();
		this.balance = balance;
	
	}
	
}
