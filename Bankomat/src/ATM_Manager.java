import java.util.Scanner;

public class ATM_Manager {
	public void startATM() {
		Scanner sc = new Scanner(System.in);
		double amount = 0;
		int operation = 0;
		boolean bcontinue = true;
		ATM atm1 = new ATM(0);
		Controller controller1 = new Controller();
		ConsoleOutput conOut = new ConsoleOutput();
		int balance = 0;

		do {
		System.out.println(
				"Choose your Operation. Type '1' to check your balance. Type '2' to withdraw. Type '3' to deposit. Type '4' to shut down the ATM.");
		operation = sc.nextInt();
		switch (operation) {
			case 1: 
				conOut.outputBalance(atm1.checkBalance());
					break;
			case 2:
				System.out.println("enter the amount you want to withdraw");
				amount = sc.nextDouble();
				if(atm1.checkBalance() >= amount) {
				atm1.withdrawMoney(amount);
				conOut.outputWithdrawMessage(atm1.checkBalance(), amount);
				}else {
					System.out.println(atm1.insufficientBalance());
				}
					break;
			case 3:
				System.out.println("enter the amount you want to deposit");
				amount = sc.nextDouble();
				atm1.depositMoney(amount);
				conOut.outputDepositMessage(atm1.checkBalance(), amount);
					break;
					
			case 4:
				System.out.println("Shutting Down");
				bcontinue = false;
					break;
				
			default:
				System.out.println("Please choose a valid operation");
					break;

		
		}
	}while (bcontinue == true);
	}
}
