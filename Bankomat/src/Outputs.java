
public interface Outputs {
	public void outputBalance(double balance);
	public void outputWithdrawMessage(double balance, double amount);
	public void outputDepositMessage(double balance, double amount);
}
