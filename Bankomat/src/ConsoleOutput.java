
public class ConsoleOutput implements Outputs{
	
	public void outputBalance(double balance) {
		String returnMessage = "Your balance is: "+ balance +"�";
		System.out.println(returnMessage);
	}
	public void outputWithdrawMessage(double balance, double amount) {
		double newBalance = balance;
		String withdrawMessage = "you have withdrawn " + amount +"�";
		String newBalanceMessage = "Your new Balance is: " + newBalance +"�";
		System.out.println(withdrawMessage);
		System.out.println(newBalanceMessage);
	}
	public void outputDepositMessage(double balance, double amount) {
		String withdrawMessage = "you have deposited " + amount +"�";
		String newBalanceMessage = "Your new Balance is: " + balance + "�";
		System.out.println(withdrawMessage);
		System.out.println(newBalanceMessage);
	}
}
